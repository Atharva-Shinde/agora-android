package org.aossie.agoraandroid.data.network.responses

import org.aossie.agoraandroid.data.db.model.Winner

data class Winners(
  val winners: List<Winner>
)
